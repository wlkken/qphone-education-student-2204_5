const $ = require('../../utils/utils.js');
// pages/coursedesc/coursedesc.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cid: null
  },

  contentConv(content) {
     return content.replace(/<img[^>]*>/gi, function (match, capture) {
          return match.replace(/style\s*?=\s*?([‘"])[\s\S]*?\1/ig, 'style="max-width:100%;height:auto;"') // 替换style
     });
  },

  /**
   * 查询课程详情
   */
  queryCourseDesc(){
    $.ajax({
      url: "/course/queryCourseById",
      data: {
        cid: this.data.cid
      },
      success: (data) => {
        data.info = this.contentConv(data.info);

        // var descs = data.descs;
        // for(var i = 0; i < descs.length; i++){
        //   var beginTime = descs[i].descBeginTime;
        //   var endTime = descs[i].descEndTime;

        //   var begin = new Date(beginTime);
        //   var end = new Date(endTime);

        //   //获得日期
        //   var y = begin.getFullYear();
        //   var m = begin.getMonth() + 1;
        //   var d = begin.getDate();
        //   descs[i].date = y + "-" + this.timeFormat(m) + "-" + this.timeFormat(d);

        //   var h = begin.getHours();
        //   var m = begin.getMinutes();
        //   descs[i].begin = this.timeFormat(h) + ":" + this.timeFormat(m);

        //   var h2 = end.getHours();
        //   var m2 = end.getMinutes();
        //   descs[i].end = this.timeFormat(h2) + ":" + this.timeFormat(m2);
        // }

        this.setData({
          course: data
        });
      }
    });
  },

  /**
   * 格式化
   */
  timeFormat(t){
    return t < 10 ? "0" + t : t;
  },

  /**
   * 购买课程
   */
  buyCourse(){
    $.ajax({
      url: "/course/buy",
      data: {
        cid: this.data.cid
      },
      success: (data) => {
        wx.showToast({
          title: "购买成功",
          icon: "success"
        });
      }
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var cid = options.cid;
    this.setData({
      cid: cid
    });

    //调用查询方法
    this.queryCourseDesc();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})