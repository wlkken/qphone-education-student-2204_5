//导入工具包
const $ = require("../../utils/utils")

// pages/me/me.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    islogin: false,
    header: "/static/icon/me.png",
    nickName: "未登录",
    score: 0,
    show: false,
    type: 0,
    redscore: 0,
    count: 0,
    info: "大吉大利 今晚吃鸡",
    sendStatus: false,
    redid: null
  },

  /**
   * 登录
   */
  login(){

    //获取用户的头像和昵称 - 传递给后端保存到数据库中
    wx.getUserProfile({
      desc: "用于小程序登录",
      success: (data) => {
        var nickName = data.userInfo.nickName;
        var header = data.userInfo.avatarUrl;

        //进行小程序端登录的操作
        wx.login({
          success: (data) => {
            //将code传递给后端
            $.ajax({
              type: "POST",
              url: "/auth/login",
              data: {
                wechatCode: data.code,
                header: header,
                nickName: nickName
              },
              success: (data) => {
                console.log("登录成功！", data);
                //保存登录凭证
                wx.setStorageSync('loginToken', data);
                wx.setStorageSync('header', header);
                wx.setStorageSync('nickName', nickName);
                //显示昵称和头像
                this.setData({
                  header: header,
                  nickName: nickName,
                  islogin: true
                });
                //主动查询积分
                this.queryUserScore();
              }
            });
          }
        });
      }
    });
  },

  /**
   * 查询用户积分
   */
  queryUserScore(){
    $.ajax({
      url: "/user/stu/querySocre",
      page: this,
      success: (data) => {
        this.setData({
          score: data
        });
      }
    });
  },

  /**
   * 弹出红包的弹出框
   */
  openDialog(){
    this.setData({
      show: true
    });
  },

  selectChange(data){
    console.log("红包类型", data.detail.value);
    this.setData({
      type: data.detail.value
    });
  },

  scoreInput(data){
    console.log("红包积分额度", data.detail.value);
    this.setData({
      redscore: data.detail.value
    });
  },

  countInput(data){
    console.log("红包份数", data.detail.value);
    this.setData({
      count: data.detail.value
    });
  },

  infoInput(data){
    console.log("红包备注", data.detail.value);
    this.setData({
      info: data.detail.value
    });
  },
  
  //发送红包
  sendRed(){
    //参数校验
    if(!this.data.redscore){
      wx.showToast({
        title: '红包积分不能为空',
        icon: "none"
      });
      return;
    }

    if(!this.data.count){
      wx.showToast({
        title: '红包份数不能为空',
        icon: "none"
      });
      return;
    }

    if(parseInt(this.data.redscore) < parseInt(this.data.count)){
      wx.showToast({
        title: '积分的数量不能少于红包份数',
        icon: "none"
      });
      return;
    }

    if(this.data.type == 0 && (this.data.redscore % this.data.count != 0)){
      wx.showToast({
        title: '固定红包份额必须是数量的整数倍',
        icon: "none"
      });
      return;
    }

    $.ajax({
      type: "POST",
      url: "/red/send",
      data: {
        type: this.data.type,
        score: this.data.redscore,
        count: this.data.count,
        info: this.data.info,
      },
      success: (redid) => {
        console.log("红包发送成功...", redid);
        //关闭弹窗
        this.setData({
          redid: redid,
          sendStatus: true
        });
      }
    });

  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    //获取登录令牌
    var loginToken = wx.getStorageSync('loginToken');
    if(loginToken){
      //已经登录
      this.setData({
        islogin: true,
        header: wx.getStorageSync('header'),
        nickName: wx.getStorageSync('nickName')
      });

      //主动查询积分
      this.queryUserScore();
    } else {
      //没有登录 - 重置状态
      this.setData({
        islogin: false,
        header: "/static/icon/me.png",
        nickName: "未登录"
      });
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: this.info,
      imageUrl: "/static/img/redfengmian.jpeg",
      path: "/pages/robred/robred?redid=" + this.data.redid
    }
  }
})