//导入工具包
const $ = require("../../utils/utils")

// pages/searchresult/searchresult.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    courses: []//课程列表
  },

  /**
   * 点击课程列表，进入课程详情页面 
   */
  courseDesc(data){
    console.log("----->点击了", data.currentTarget.dataset.course.id);

    //跳转到课程详情页，展示课程详细信息（作业）
    wx.navigateTo({
      url: '../coursedesc/coursedesc?cid=' + data.currentTarget.dataset.course.id
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var keyword = options.keyword;

    //发送请求
    $.ajax({
      type: "POST",
      url: "/search/courseSearch",
      data: {
        keyword: keyword
      },
      success: (data) => {
        console.log("搜索的结果数量：", data);
        this.setData({
          courses: data
        });
      }
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})