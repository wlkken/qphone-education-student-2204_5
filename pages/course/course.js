const $ = require('../../utils/utils.js');

// pages/course/course.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    courses: []
  },

  /**
   * 查询当前用户的购买的课程列表
   */
  queryMyCourse(){
    $.ajax({
      url: "/course/queryMyCourse",
      success: (data) => {
        this.setData({
          courses: data
        });
      }
    });
  },

  /**
   * 开始上课
   */
  inPlayer(data){
    var course = data.currentTarget.dataset;
    console.log("进入直播间...", course);
    if(course.online == 0){
      wx.showToast({
        title: '当前课程还未上课....',
        icon: "none"
      });
    } else {
      //当前课程已经开始上课，可以进入直播间
      wx.navigateTo({
        url: '/pages/courseplayer/courseplayer?cid=' + course.cid
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.queryMyCourse();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})