//导入工具包
const $ = require("../../utils/utils")

// pages/robred/robred.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    redid: null,
    loginPage: false,
    robinfo: "拼抢中...."
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var redid = options.redid ? options.redid : 23;
    console.log("触发抢红包的请求...", redid);
    this.setData({
      redid: redid
    });

    //主动发送一次抢红包的请求
    this.robred();
  },

  //抢红包的方法
  robred(){
    //发送ajax请求
    $.ajax({
      type: "GET",
      url: "/red/robred",
      data: {
        redid: this.data.redid
      },
      success: (data) => {
        switch(data) {
          case -1:
            this.setData({
              robinfo: "红包信息有误，请不要乱来！"
            });
            break;
          case -2:
            this.setData({
              robinfo: "手慢了，红包已经抢完！"
            });
            break;
          case -3:
            this.setData({
              robinfo: "你已经抢过该红包！"
            });
            break;
          default:
            this.setData({
              robinfo: "恭喜你，抢到" + data + "积分！"
            });
            break;    
        }
      },
      authHandler: () => {
        //认证失败后的处理方式
        console.log("抢红包认证未通过.....");
        this.setData({
          loginPage: true
        });
      }
    });
  },

  //小程序登录
  /**
   * 登录
   */
  login(){

    //获取用户的头像和昵称 - 传递给后端保存到数据库中
    wx.getUserProfile({
      desc: "用于小程序登录",
      success: (data) => {
        var nickName = data.userInfo.nickName;
        var header = data.userInfo.avatarUrl;

        //进行小程序端登录的操作
        wx.login({
          success: (data) => {
            //将code传递给后端
            $.ajax({
              type: "POST",
              url: "/auth/login",
              data: {
                wechatCode: data.code,
                header: header,
                nickName: nickName
              },
              success: (data) => {
                console.log("登录成功！", data);

                this.setData({
                  loginPage: false
                });

                //保存登录凭证
                wx.setStorageSync('loginToken', data);
                wx.setStorageSync('header', header);
                wx.setStorageSync('nickName', nickName);
                
                //重新发送一次抢红包的请求
                this.robred();
              }
            });
          }
        });
      }
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})