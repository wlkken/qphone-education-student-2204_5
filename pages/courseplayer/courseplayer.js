const $ = require('../../utils/utils.js');

// pages/courseplayer/courseplayer.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cid: "",
    url: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //获取当前需要进入的直播间的课程id
    console.log("进入直播间...." + options.cid);
    this.setData({
      cid: options.cid,
      url: $.baseUrl + ":8080/#/CourseLooker/" + options.cid + "/" + wx.getStorageSync('loginToken')
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})