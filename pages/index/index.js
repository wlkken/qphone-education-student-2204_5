// pages/index/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isLoading: false,
    sec: 5,
    logos: [
      "/static/img/logo1.jpg",
      "/static/img/logo2.jpg",
      "/static/img/logo3.jpg"
    ]
  },

  /**
   * 跳过按钮的倒计时
   */
  djs(){
    if(this.data.sec <= 0){
      //结束了
      this.skip();
      return;
    }

    setTimeout(() => {
      //修改data中sec的值
      this.setData({
        sec: this.data.sec - 1
      });
      //递归调用
      this.djs();
    }, 1000);
  },

  //直接跳过
  skip(){
    this.setData({
      sec: 0,
      isLoading: false
    });

    //显示tabbar
    wx.showTabBar({
      animation: false
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.djs();

    if(this.data.isLoading){
      //加载广告时，隐藏tabbar
      wx.hideTabBar({
        animation: false,
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})