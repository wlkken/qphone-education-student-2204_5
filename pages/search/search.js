//导入工具包
const $ = require("../../utils/utils")

// pages/search/search.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    searchKeyWord: null
  },

  /**
   * 进行搜索
   */
  search(){
    console.log("开始进行搜索...." + this.data.searchKeyWord);

    //进行页面的跳转
    wx.navigateTo({
      url: '/pages/searchresult/searchresult?keyword=' + this.data.searchKeyWord,
    })
  },

  /**
   * 输入框的方法绑定
   */
  searchInput(e){
    this.setData({
      searchKeyWord: e.detail.value
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})