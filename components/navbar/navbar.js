// components/navbar/navbar.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isback: {
      type: Boolean,
      value: false
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    backIcon: "<"
  },

  /**
   * 组件的方法列表
   */
  methods: {
    backPage(){
      wx.navigateBack({
        delta: -1
      })
    }
  }
})
