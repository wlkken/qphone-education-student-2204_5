const baseUrl = "http://localhost";

//定义了一个方法变量，变量名为ajax
const ajax = (param) => {

  //开启等待框
  wx.showLoading({
    title: '数据加载中...',
  })

  //处理请求头的数据
  var headerObj = param.header ? param.header : {};
  //修改请求头的格式，以表单的形式提交数据
  headerObj["Content-Type"] = "application/x-www-form-urlencoded"; 

  //请求头处理令牌
  var loginToken = wx.getStorageSync('loginToken');
  if(loginToken) {
    headerObj.loginToken = loginToken;
  }

  headerObj.fromType = 1;


  //发送ajax请求后端的数据
  wx.request({
    method: param.type ? param.type : "GET",
    url: baseUrl + param.url,
    data: param.data ? param.data : {},
    header: headerObj,
    success: (result) => {
      if(result.statusCode == 200) {
        //判断开发者是否传递了成功的处理方法
        if(param.success){
          //调用开发者的success
          param.success(result.data.data);
        }
      } else {
        //失败
        var r = result.data;
        if(r.code == 406) {
          //认证异常，删除本地令牌 重新登录
          wx.removeStorageSync('loginToken');
          
          //认证失败后的处理方式
          if(param.authHandler){
            param.authHandler();
          } else {
            //弹出提示框
            wx.showToast({
              title: "认证已过期",
              icon: "error"
            });

            //尝试刷新页面
            if(param.page){
              param.page.onShow();
            }
          }
        } else {
          //异常默认的处理方式
          //弹出提示框
          wx.showToast({
            title: r.msg,
            icon: "error"
          });
        }
      }
    },
    complete: () => {
      //关闭等待框
      wx.hideLoading({
        success: (res) => {},
      })
    }
  })
};

//声明工具变量
module.exports = {
  ajax,
  baseUrl
}
